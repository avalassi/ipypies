#!/bin/bash

source /afs/cern.ch/sw/lcg/releases/LCG_74root6/ROOT/6.02.05/x86_64-cc7-gcc48-opt/bin/thisroot.sh    
ipython notebook --no-browser --port ${1-8888}
