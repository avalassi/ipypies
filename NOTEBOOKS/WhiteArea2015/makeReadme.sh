#! /bin/bash
cd `dirname $0`
\rm -f README.md
echo "Use http://nbviewer.ipython.org to view the notebooks in this directory:" > README.md
for file in `find . -name '*.ipynb' | sort | grep -v .ipynb_checkpoints`; do
  echo "- http://nbviewer.ipython.org/urls/gitlab.cern.ch/avalassi/ipypies/raw/master/NOTEBOOKS/${PWD#*NOTEBOOKS/}/${file#./}?flush_cache=true" >> README.md
done
