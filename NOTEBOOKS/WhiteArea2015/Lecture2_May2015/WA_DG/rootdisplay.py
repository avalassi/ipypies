"""
Helper module for displaying ROOT pad in ipython notebooks

@author domenico.giordano@cern.ch

This is an adaptation of the helper developed by
alexander.mazurov@cern.ch
andrey.ustyuzhanin@cern.ch
Ref: http://mazurov.github.io/webfest2013/

Usage example:
    # Save this file as rootdisplay.py to your working directory.
    
    import rootnotes
    c1 = TCanvas("c1")
    fun1 = TF1( 'fun1', 'abs(sin(x)/x)', 0, 10)
    fun1.Draw()
    c1.display()
"""


import tempfile
from IPython.core import display
from ROOT import gPad, TPad, TCanvas
from ROOT import TIter, gROOT

def ROOTIPythonDisplay(self,figFormat='svg'):
    file = tempfile.NamedTemporaryFile(suffix=".%s"%(figFormat))
    self.SaveAs(file.name)
    img = 'Allowed formats are svg, jpg, png'
    if figFormat == 'svg':
        img = display.SVG(filename=file.name)
    elif figFormat == 'jpg' or figFormat == 'png':
        img = display.Image(filename=file.name, embed=True)
    file.close()
    return img

TPad.display = ROOTIPythonDisplay
TCanvas.display = ROOTIPythonDisplay


def FindCanvas(name=False,title=False):
    canvasList = gROOT.GetListOfCanvases()
    obj = None 
    if name :
        obj = canvasList.FindObject(name)
    elif title :
        next = TIter(canvasList)            
        obj = next() 
        while (obj and obj.GetTitle() != title):
            obj = next() 
    return obj  
