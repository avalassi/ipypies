#!/bin/sh
echo "You must comment out this line!"; exit 1

cd /home/avalassi/WA/hassen/JSON
for file in cern ral ; do
  # Split the file into separate lines (otherwise sed chokes!)
  \rm -f ${file}_fmt.txt
  time fmt -s -w1 ../RAW/$file.txt > ${file}_fmt.txt # approx 120s each file
  # Then convert to JSON format
  \rm -f $file.json
  echo '{"transfers":' > $file.json
  time cat ${file}_fmt.txt | sed -e "s/}\]\[{/},\n{/g" -e "s/\]\[//g" -e "s/\[{/\[\n{/" -e "s/}\]/}\n\]/" -e 's/'\''/"/g' -e 's/None,/null,/' >> $file.json # approx 180s each file
  echo "}" >> $file.json
  # Create smaller 300MB files with only 1M transfers i.e. 16M+2 lines
  # (one transfer record is 16 lines, also include the first 2 header lines)
  \rm -f ${file}1M.json
  time head -16000001 $file.json > ${file}1M.json # first 16M+1
  time awk 'NR == 16000002{print; exit}' $file.json | sed 's/,$//' >> ${file}1M.json # delete trailing comma on 16M+2th line
  echo "]" >> ${file}1M.json
  echo "}" >> ${file}1M.json
done
