#!/bin/sh
echo "You must comment out this line!"; exit 1

cd `dirname $0`

# Extract a smaller portion of the file
head -c926 /home/avalassi/WA/hassen/RAW/cern.txt > cern_short.txt
\cp cern_short.txt cern_short2.txt
echo "]" >> cern_short.txt
echo "," >> cern_short2.txt
echo "]" >> cern_short2.txt

for file in cern_short cern_short2; do
  # Split the file into separate lines (otherwise sed chokes!)
  \rm -f ${file}_fmt.txt
  time fmt -s -w1 $file.txt > ${file}_fmt.txt
  # Then convert to JSON format
  \rm -f $file.json
  echo '{"transfers":' > $file.json
  time cat ${file}_fmt.txt | sed -e "s/}\]\[{/},\n{/g" -e "s/\]\[//g" -e "s/\[{/\[\n{/" -e "s/}\]/}\n\]/" -e 's/'\''/"/g' -e 's/None,/null,/' >> $file.json
  echo "}" >> $file.json
done
