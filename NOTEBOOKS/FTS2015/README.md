Use http://nbviewer.ipython.org to view the notebooks in this directory:
- http://nbviewer.ipython.org/urls/gitlab.cern.ch/avalassi/ipypies/raw/master/NOTEBOOKS/FTS2015/analysis1.ipynb?flush_cache=true
- http://nbviewer.ipython.org/urls/gitlab.cern.ch/avalassi/ipypies/raw/master/NOTEBOOKS/FTS2015/analysis2_1fetchdata.ipynb?flush_cache=true
- http://nbviewer.ipython.org/urls/gitlab.cern.ch/avalassi/ipypies/raw/master/NOTEBOOKS/FTS2015/analysis2_2analyse.ipynb?flush_cache=true
- http://nbviewer.ipython.org/urls/gitlab.cern.ch/avalassi/ipypies/raw/master/NOTEBOOKS/FTS2015/simple_tests/test_json_and_pandas.ipynb?flush_cache=true
