#!/bin/bash

topdir=`dirname $0`
topdir=`cd $topdir; pwd -P`

privdir=`cd; pwd -P`/private
certfile=$privdir/ipypies/mycert.pem
sha1file=$privdir/ipypies/mysha1.txt

# Default arguments
port=8888  # default: port is 8888
asuser=""  # default: start notebook server as the current user 
usepswd=0  # default: notebook server uses http without a password
if [ `whoami` == "avalassi" ]; then asuser=ipythonsrv; usepswd=1; fi

# Read optional arguments
usage() { 
  echo "Usage: $0 [-P <port>] [-u <user>] [-s <0|1>]" 1>&2 
  echo "Example: $0 -uipythonsrv -s1" 1>&2 
  echo "Example: $0 -u \"\" -s0" 1>&2 
  echo "Optional arguments: " 1>&2 
  echo "  -P <port>  Start ipython notebook on port <port>" 1>&2 
  echo "             [Default: -P8888]" 1>&2 
  echo "  -u <user>  Start ipython notebook as user <user> (using sudo)" 1>&2 
  echo "             Destroy Kerberos/AFS credentials of the current user" 1>&2 
  echo "             [Default: -u\"\" i.e. start notebook as \$USER]" 1>&2 
  echo "  -s <0|1>   0: Insecure notebook with http and no password" 1>&2 
  echo "             1: Secure notebook with https and password" 1>&2 
  echo "             Location of certificate file for https:" 1>&2 
  echo "               $certfile" 1>&2 
  echo "             Location of sha1-encrypted password file:" 1>&2 
  echo "               $sha1file" 1>&2 
  echo "             Recreate both files if either of them is missing" 1>&2 
  echo "             [Default: -s0]" 1>&2 
  exit 1; 
}
while getopts "P:u:s:" o; do
    case "${o}" in
        P)
            port="${OPTARG}"
            ;;
        u)
            asuser="${OPTARG}"
            ;;
        s)
            usepswd="${OPTARG}"
            if [ "$usepswd" != "0" ] && [ "$usepswd" != "1" ]; then usage; fi
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
if [ "$*" != "" ]; then usage; fi 
echo "Received arguments: -P${port} -u${asuser} -s${usepswd}"

# Check if the specified port is valid 
if ! [[ "$port" =~ ^[0-9]*$ ]]; then 
  echo "ERROR! Invalid port '$port'"
  exit 1
fi

# Check if the specified username is valid 
if [ "$asuser" != "" ]; then
  if ! [[ "$asuser" =~ ^[a-z][-a-z0-9_]*$ ]]; then 
    echo "ERROR! Invalid username '$asuser'"
    exit 1
  fi
fi

# Determine the command to check if the port is already in use
which nmap > /dev/null 2>&1
if [ "$?" == "0" ]; then
  nmap=nmap # Syntax ok on SLC6 and CC7, but this is missing on lxplus6
else
  nmap=nc # Syntax "nc -z" fails on CC7, but this is only needed on lxplus6
fi

# Check if the port is already in use
port=8888
if [ "$nmap" == "nmap" ]; then
  nmap -p $port localhost | grep $port | grep open > /dev/null 
else
  nc -z localhost $port > /dev/null
fi
if [ "$?" == "0" ]; then
  echo "ERROR! Port $port is already in use"
  echo "Try to type 'screen -D -r' to attach"
  exit 1
fi

# Create a screenlog directory if it does not exist
logdir=$topdir/.screenlogs/`hostname`
if [ "$asuser" == "" ]; then
  logdir=$logdir/`whoami`
else
  logdir=$logdir/$asuser
fi 
if [ ! -d $logdir ]; then mkdir -p $logdir; fi

# Recreate the .ipython directory from scratch
# This may help to solve catastrophic issues (e.g. "database corruption")
# This may also help to avoid issues with expired tokens if ~/.ipython is on AFS
# [Note: avoid mkdir -p that would set the wrong mask for getfacl]
ipydir=$topdir/.ipython
if [ ! -d $ipydir ]; then mkdir $ipydir; fi # avoid 'mkdir -p'
if [ "$asuser" == "" ]; then
  ipydir=$ipydir/`whoami`
else
  ipydir=$ipydir/$asuser
fi 
if [ ! -d $ipydir ]; then mkdir $ipydir; fi # avoid 'mkdir -p'
ipydir=$ipydir/`hostname`
if [ "$asuser" != "" ]; then
  sudo \rm -rf $ipydir/profile_default # created as another user
fi
\rm -rf $ipydir
mkdir $ipydir # avoid 'mkdir -p'
if [ "$asuser" != "" ]; then
  sudo sudo -u $asuser mkdir $ipydir/profile_default # avoid 'mkdir -p'
  sudo sudo -u $asuser mkdir $ipydir/profile_default/security # avoid 'mkdir -p'
else
  mkdir $ipydir/profile_default # avoid 'mkdir -p'
  mkdir $ipydir/profile_default/security # avoid 'mkdir -p'
fi

# Check if the certificate and password files exist, else recreate them
if [ "$usepswd" == "1" ]; then
  if [ ! -f $certfile ] || [ ! -f $sha1file ]; then
     echo "WARNING! $certfile and/or $sha1file do not exist"
     echo "WARNING! both files will be (re-)created"
     $topdir/create_certificate_and_password.sh 
  fi
  if [ `awk 'END{print NR}' $sha1file` != "1" ]; then
    echo "ERROR! $sha1file should contain a single line"
    exit 1
  fi
  ipypswdpy=$ipydir/profile_default/security/ipython_password.py
  certfile2=$ipydir/profile_default/security/`basename $certfile`
  \cp $certfile $certfile2
  echo "c = get_config()" > $ipypswdpy
  echo "c.NotebookApp.password='`cat $sha1file`'" >> $ipypswdpy
  usepswd="--NotebookApp.certfile=$certfile2 --config=$ipypswdpy"
else
  usepswd=""
fi

# Go to the IPython notebook directory before starting IPython
nbkdir=$topdir/NOTEBOOKS
if [ ! -d $nbkdir ]; then
  echo "ERROR! Directory $nbkdir not found"
  exit 1
fi
if fs la $nbkdir > /dev/null 2>&1; then
  echo "WARNING! The NOTEBOOKS directory is on AFS"
  echo "WARNING! You will need to reattach to screen and kinit periodically..."
fi

# Start IPython within a screen session
# Set the environment within screen becuase screen unsets LD_LIBRARY_PATH!
# [See http://superuser.com/questions/235760/ld-library-path-unset-by-screen]
# [See http://ubuntuforums.org/showthread.php?t=1545643]
if [ ! -f $topdir/setup_lcg_ipython.sh ]; then
  echo "ERROR! Script $topdir/setup_lcg_ipython.sh not found"
  exit 1
fi
cd $logdir
log=`pwd -P`/screenlog.0
\rm -f $log > /dev/null 2>&1
header="=================================================================="
# Echoing header to screenlog also works around the missing first character bug
# See http://savannah.gnu.org/bugs/?34200
echo "INFO: starting ipython (logfile available at $log)"
if [ "$asuser" == "" ]; then
  screen -L -S ipython -d -m bash -c "cd $nbkdir; echo $header; if . ../setup_lcg_ipython.sh; then ipython notebook $cert --ipython-dir=$ipydir --port $port --no-browser $usepswd; else exit 1; fi"
else
  screen -L -S ipython -d -m sudo sudo -u $asuser bash -c "cd $nbkdir; echo $header; if . ../setup_lcg_ipython.sh; then ipython notebook $cert --ipython-dir=$ipydir --port $port --no-browser $usepswd; else exit 1; fi"
fi
sleep 1 # This will normally leave enough time for the screenlog file to appear
if [ ! -f $file ]; then
  echo "WARNING! No such file: $log"
else
  # Wait for the first characters to appear in the screenlog, then dump them
  while [ `stat -c %s $log` == "0" ]; do sleep 1; done
  cat $log
  echo $header
fi
if [ "$nmap" == "nmap" ]; then
  nmap -p $port localhost | grep $port | grep open > /dev/null 
else
  nc -z localhost $port > /dev/null
fi
if [ "$?" != "0" ]; then
  echo "ERROR! ipython failed!"
  exit 1
else
  echo "INFO: ipython started on port $port"
  echo "When detached, type 'screen -list' to list"
  echo "When detached, type 'screen -wipe' to wipe (after kill)"
  echo "When detached, type 'screen -D -r' to reattach"
  echo "When attached, type 'CTRL-A d' to detach"
fi
