# Do not remove this line
if [ "1" != "1" ]; then echo "Force failure and exit if sourced from csh"; fi

# Check O/S version
if [ ! -f /etc/redhat-release ]; then
  echo "ERROR! O/S is not a RedHat-based Linux system"
  echo "ERROR! This script is only supported on SLC6 and CC7"
  return 1
elif egrep -q "^Scientific Linux CERN SLC release 6" /etc/redhat-release; then
  os=slc6
elif egrep -q "^CentOS Linux release 7" /etc/redhat-release; then
  os=cc7
else
  echo "ERROR! Unknown O/S '"`more /etc/redhat-release`"'"
  echo "ERROR! This script is only supported on SLC6 and CC7"
  return 1
fi

# Software versions
LCG_version='LCG_77root6'
LCGCMT_version='LCGCMT_77root6' # Compare to CMT setup
blas_version='20110419'
GCCXML_version='0.9.0_20131026'
lapack_version='3.5.0'
mysql_version='5.5.27'
oracle_version='11.2.0.3.0'
Python_version='2.7.9.p1'
Python_twodigit='python2.7'
pyanalysis_version='1.5'_$Python_twodigit
pytools_version='1.9'_$Python_twodigit
ROOT_version='6.02.10'
xrootd_version='3.3.6'

# Platform (O/S and compiler)
LCG_platform=x86_64-${os}-gcc48-opt
gcc_version='4.8.1'

#-----------------------------------------------------------------------------

LCG_releases=/afs/cern.ch/sw/lcg/releases/$LCG_version
###LCG_releases=/afs/cern.ch/sw/lcg/releases/LCGCMT/$LCGCMT_version/LCG_Settings/../../../$LCG_version/ # Compare to CMT setup (keep the trailing slash!)
echo "INFO: setting up ipython from $LCG_releases"
if [ ! -d $LCG_releases ]; then
  echo "ERROR! No such directory: $LCG_releases"
  return 1
fi

# Software installations
blas_home=$LCG_releases/blas/$blas_version/$LCG_platform
GCCXML_home=$LCG_releases/gccxml/$GCCXML_version/$LCG_platform
lapack_home=$LCG_releases/lapack/$lapack_version/$LCG_platform
mysql_home=$LCG_releases/mysql/$mysql_version/$LCG_platform
oracle_home=$LCG_releases/oracle/$oracle_version/$LCG_platform
pyanalysis_home=$LCG_releases/pyanalysis/$pyanalysis_version/$LCG_platform
Python_home=$LCG_releases/Python/$Python_version/$LCG_platform
pytools_home=$LCG_releases/pytools/$pytools_version/$LCG_platform
ROOT_base=$LCG_releases/ROOT/$ROOT_version
ROOT_home=$ROOT_base/$LCG_platform
xrootd_home=$LCG_releases/xrootd/$xrootd_version/$LCG_platform
gcc_home=$LCG_releases/gcc/$gcc_version/x86_64-${os}

# Set up compiler
export LD_LIBRARY_PATH=$gcc_home/lib64:$LD_LIBRARY_PATH
export COMPILER_PATH=$gcc_home/lib/gcc/x86_64-unknown-linux-gnu/$gcc_version
export PATH=$gcc_home/bin:$PATH

# Set up Python
export LD_LIBRARY_PATH=$Python_home/lib:$LD_LIBRARY_PATH
export MANPATH=$Python_home/share/man:$MANPATH
export PATH=$Python_home/bin:$PATH
export ROOT_INCLUDE_PATH=$Python_home/include/$Python_twodigit # First in path

# Set up blas and lapack (needed by pyanalysis)
export BLAS=$blas_home
export LAPACK=$lapack_home
export LD_LIBRARY_PATH=$blas_home/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$lapack_home/lib:$LD_LIBRARY_PATH

# Set up pyanalysis
export LD_LIBRARY_PATH=$pyanalysis_home/lib:$LD_LIBRARY_PATH
export MANPATH=$pyanalysis_home/man:$MANPATH
export PATH=$pyanalysis_home/bin:$PATH
export PYTHONPATH=$pyanalysis_home/lib/$Python_twodigit/site-packages:$PYTHONPATH

# Set up mysql (needed by pytools)
export LD_LIBRARY_PATH=$mysql_home/lib:$LD_LIBRARY_PATH
export MANPATH=$mysql_home/man:$MANPATH
export PATH=$mysql_home/bin:$PATH
export ROOT_INCLUDE_PATH=$mysql_home/include:$ROOT_INCLUDE_PATH

# Set up pytools
export LD_LIBRARY_PATH=$pytools_home/lib:$LD_LIBRARY_PATH
export PATH=$pytools_home/bin:$PATH
export PYTHONPATH=$pytools_home/lib/$Python_twodigit/site-packages:$PYTHONPATH

# Set up gccxml (needed by ROOT)
export MANPATH=$GCCXML_home/share/man:$MANPATH
export PATH=$GCCXML_home/bin:$PATH

# Set up xrootd (needed by ROOT)
export LD_LIBRARY_PATH=$xrootd_home/lib64:$LD_LIBRARY_PATH
export PATH=$xrootd_home/bin:$PATH
export ROOT_INCLUDE_PATH=$xrootd_home/include:$ROOT_INCLUDE_PATH

# Set up ROOT
export ROOTSYS=$ROOT_home
export LD_LIBRARY_PATH=$ROOT_home/lib:$LD_LIBRARY_PATH
export MANPATH=$ROOT_home/man:$MANPATH:$ROOT_base/src/root/man
export PATH=$ROOT_home/bin:$PATH
export PYTHONPATH=$ROOT_home/lib:$PYTHONPATH

# Set up oracle
export LD_LIBRARY_PATH=$oracle_home/lib:$LD_LIBRARY_PATH
export PATH=$oracle_home/bin:$PATH
export ROOT_INCLUDE_PATH=$oracle_home/include:$ROOT_INCLUDE_PATH
export ORA_FPU_PRECISION='EXTENDED'
export NLS_LANG='AMERICAN_AMERICA.WE8ISO8859P1'
export TNS_ADMIN='/afs/cern.ch/project/oracle/admin/'

#-----------------------------------------------------------------------------

# Additional private patches over LCG AFS installation (if needed, e.g. pandas)
eggtopdir=/afs/cern.ch/sw/lcg/app/releases/CORAL/internal/PythonEggs
export PYTHONPATH=$eggtopdir/$LCG_version/$LCG_platform:$PYTHONPATH
