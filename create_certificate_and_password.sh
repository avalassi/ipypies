#!/bin/bash

topdir=`dirname $0`
topdir=`cd $topdir; pwd`

privdir=`cd; pwd`/private
certfile=$privdir/ipypies/mycert.pem
sha1file=$privdir/ipypies/mysha1.txt

# Set up ipython
if [ ! -f $topdir/setup_lcg_ipython.sh ]; then
  echo "ERROR! Script $topdir/setup_lcg_ipython.sh not found"
  exit 1
fi

# Check if the certificate and password directory exists, else create it
if [ ! -d $privdir ]; then
  echo "ERROR! Directory $privdir does not exist"
  exit 1
fi
mkdir -p $privdir/ipypies
if [ ! -d $privdir/ipypies ]; then
  echo "ERROR! Directory $privdir/ipypies could not be created"
  exit 1
fi

# Create the certificate file
echo "INFO: create self-signed certificate file $certfile"
if [ -f $certfile ]; then 
  echo "WARNING! $certfile exists, it will be deleted and recreated"
  #\rm -rf $certfile
fi
###openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout $certfile -out $certfile # SHA-1
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -sha256 -keyout $certfile -out $certfile # SHA-2

# Create the sha1-encrypted password
echo "INFO: create sha1-encrypted password file $sha1file"
if [ -f $sha1file ]; then 
  echo "WARNING! $sha1file exists, it will be deleted and recreated"
  \rm -rf $sha1file
fi
python -c 'from IPython.lib import passwd; sha1=passwd(); print sha1' | grep -v Passwords do not match > $sha1file
