### Overview

The IPyPies repository hosts a collection of IPython based goodies.

It started as a follow-up to the "White Area lecture" on quantitative
data analysis by Andrea Valassi and Domenico Giordano in February 2015,
https://indico.cern.ch/event/351669/

It contains some scripts to setup an IPython notebook server on SLC6
or CC7 nodes, configured with a consistent set of IPython and ROOT 
libraries from the LCG Application Area software builds on AFS at CERN.

It also contains a few IPython notebooks, including those used during
the preparation and presentation of the White Area lecture.

### Download

IPyPies is a "Public" project and can be cloned anonymously from CERN GitLab:

    git clone https://gitlab.cern.ch/avalassi/ipypies.git

### Launch an IPython notebook server

You can launch an IPython notebook server with a simple command:

    cd ipypies
    ./start_ipython_screen.sh

This will launch a notebook server on the default port 8888, running
within a "screen" session.

The server will run in "no browser" mode, so you will need to connect
your own favourite browser to port 8888 of the server host. To use 
browser clients on a different node than the one hosting the server,
you can use ssh tunnels to forward the server port 8888 to the client 
port (which can also the same or a different one, e.g. 8886). 

For instance using putty on a Windows client to connect to a Linux server:

    putty.exe <user>@<server> -L 8886:localhost:8888

Using ssh on Linux OS

```
    ssh -Y -L 8886:localhost:8888 <user>@<server>
```

Connecting the browser to the relevant port (e.g. http://localhost:8886),
you will see the contents of the ipypies/NOTEBOOKS directory. The notebooks
from the White Area lecture are in the 2015WhiteArea subdirectory.