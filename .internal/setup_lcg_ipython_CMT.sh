# Do not remove this line
if [ "1" != "1" ]; then echo "Force failure and exit if sourced from csh"; fi

# Check O/S version
if [ ! -f /etc/redhat-release ]; then
  echo "ERROR! O/S is not a RedHat-based Linux system"
  echo "ERROR! This script is only supported on SLC6 and CC7"
  return 1
elif egrep -q "^Scientific Linux CERN SLC release 6" /etc/redhat-release; then
  os=slc6
elif egrep -q "^CentOS Linux release 7" /etc/redhat-release; then
  os=cc7
else
  echo "ERROR! Unknown O/S '"`more /etc/redhat-release`"'"
  echo "ERROR! This script is only supported on SLC6 and CC7"
  return 1
fi

# Software version and platform
LCGCMT_version='LCGCMT_77root6'
LCG_platform=x86_64-${os}-gcc48-opt

# Create a temporary directory for CMT setup
tmpdir=/tmp/$USER/IPythonCMT 
\rm -rf $tmpdir 
if [ "$?" != "0" ]; then return 1; fi
mkdir -p $tmpdir
if [ "$?" != "0" ]; then return 1; fi

# Create the project.cmt file
mkdir -p $tmpdir/cmt
cat << EOF > $tmpdir/cmt/project.cmt
project IPythonCMTProject
build_strategy with_installarea
setup_strategy no_root no_config
EOF
echo "use LCGCMT ${LCGCMT_version}" >> $tmpdir/cmt/project.cmt

# Create the requirements file
mkdir -p $tmpdir/config/cmt
echo v1 > $tmpdir/config/cmt/version.cmt
cat << EOF > $tmpdir/config/cmt/requirements
package IPythonCMTPackage
use Python     * LCG_Interfaces
use pyanalysis * LCG_Interfaces # uses Python, blas, lapack
use pytools    * LCG_Interfaces # uses Python, mysql
use ROOT       * LCG_Interfaces # uses Python, GCCXML, xrootd
use oracle     * LCG_Interfaces # for cx_Oracle from pytools
EOF

# Set up CMT
export CMTCONFIG=${LCG_platform}
export CMTROOT=/afs/cern.ch/sw/contrib/CMT/v1r20p20090520
export CMTPROJECTPATH=/afs/cern.ch/sw/lcg/releases
unset CMTSITE
unset CMTBIN
unset CMTPATH
unset CMTEXTRATAGS
unset CMTUSERCONTEXT
source $CMTROOT/mgr/setup.sh

# Set up the CMT package
pushd $tmpdir/config/cmt > /dev/null 2>&1
###echo Set up the environment from `pwd`
cmt config > /dev/null 2>&1
if [ "$?" != "0" ]; then return 1; fi
source setup.sh
popd > /dev/null 2>&1

# Clean up
###\rm -rf $tmpdir 
