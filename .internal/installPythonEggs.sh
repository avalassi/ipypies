#!/bin/bash
echo "You must comment out this line!"; exit 1

topdir=`dirname $0`
topdir=`cd $topdir; pwd`

# Setup python from LCG Application Area AFS
if [ ! -f $topdir/setup_lcg_ipython.sh ]; then
  echo "ERROR! Script $topdir/setup_lcg_ipython.sh not found"
  exit 1
fi
. $topdir/setup_lcg_ipython.sh
if [ "$LCG_version" == "" ]; then
  echo "ERROR! LCG_version is not set"
  exit 1
fi
if [ "$LCG_platform" == "" ]; then
  echo "ERROR! LCG_platform is not set"
  exit 1
fi

# Define the appropriate egg directory
eggdir=/afs/cern.ch/sw/lcg/app/releases/CORAL/internal/PythonEggs/$LCG_version/$LCG_platform

# Optionally recreate the egg directory
###rm -rf $eggdir

# Install all relevant packages
mkdir -p $eggdir
export PYTHONPATH=$eggdir:$PYTHONPATH
export LIBRARY_PATH=$LD_LIBRARY_PATH
time easy_install-2.7 -d $eggdir pandas # Approx 4 minutes
time easy_install-2.7 -d $eggdir numexpr # Approx 30 seconds
time easy_install-2.7 -d $eggdir pygments # Approx 7 seconds
time easy_install-2.7 -d $eggdir mistune # Approx 3 seconds
time easy_install-2.7 -d $eggdir cython # Approx 2.5 minutes
time easy_install-2.7 -d $eggdir prettytable # Approx 2 seconds
time easy_install-2.7 -d $eggdir scikit-learn # Approx 2.5 minutes
unset LIBRARY_PATH
