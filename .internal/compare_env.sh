#!/bin/bash
cd `dirname $0`

bash -c "source setup_lcg_ipython_CMT.sh; env | sort | egrep '^(COMPILER_PATH|LD_LIBRARY_PATH|MANPATH|PATH|PYTHONPATH|ROOT_INCLUDE_PATH)=' > envCMT" # CMT
bash -c "source setup_lcg_ipython.sh; env | sort | egrep '^(COMPILER_PATH|LD_LIBRARY_PATH|MANPATH|PATH|PYTHONPATH|ROOT_INCLUDE_PATH)=' > envSLN" # standalone

files="envCMT envSLN"
for env in COMPILER_PATH LD_LIBRARY_PATH MANPATH PATH PYTHONPATH ROOT_INCLUDE_PATH ; do
  sln=""
  for file in $files; do
    cmt=$sln
    sln=`cat $file | grep ^${env}= | sed "s/^${env}=//"`
  done
  echo "=== Compare $env"
  cmt=`echo "$cmt" | sed "s|/:|:|g" | tr ":" "\n" | grep LCG_77root6 | sort`
  cmt=`echo "$cmt" | sed 's|releases/LCGCMT/LCGCMT_77root6/LCG_Settings/../../../LCG_77root6|releases/LCG_77root6|g'`
  cmt=`echo "$cmt" | tr "\n" ":"`
  sln=`echo "$sln" | tr ":" "\n" | grep LCG_77root6 | grep -v PythonEggs | sort`
  sln=`echo "$sln" | tr "\n" ":"`
  if [ "$cmt" != "$sln" ]; then
    ###echo "CMT:'$cmt'"; echo; echo "SLN:'$sln'"
    echo $cmt | tr ":" "\n"; echo; echo $sln | tr ":" "\n"
  else
    echo OK
  fi
done
\rm -f $files
